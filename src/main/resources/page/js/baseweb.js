





function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null){
		var v = r[2];
		return decodeURIComponent(v);
	}
		
	return null;
}